#! /usr/bin/env python
import rospy
import time
import actionlib
from action_quiz.msg import CustomActionMsgAction, CustomActionMsgActionGoal, CustomActionMsgActionFeedback
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty

class ArdroneServerClass(object):
    # create messages that are used to publish feedback/result
    _feedback = CustomActionMsgActionFeedback()

    def __init__(self):
        self.action_name = 'action_custom_msg_as'

    def initialize_server(self):
        # creates the action server
        self._as = actionlib.SimpleActionServer(self.action_name, CustomActionMsgAction, self.goal_callback, False)
        self.pub_takeoff = rospy.Publisher('/drone/takeoff', Empty, queue_size=1)
        self.pub_land = rospy.Publisher('/drone/land', Empty, queue_size=1)
        self._as.start()

    def goal_callback(self, request):
        while self.pub_land.get_num_connections() == 0 or self.pub_takeoff.get_num_connections() == 0:
            rospy.sleep(0.5)

        success = True
        action = request.goal
        if action == 'TAKEOFF':
            self.pub_takeoff.publish(Empty())
            rospy.sleep(1)
            self._feedback.feedback = action
            self._as.publish_feedback(self._feedback)
        elif action == 'LAND':
            self.pub_land.publish(Empty())
            rospy.sleep(1)
            self._feedback.feedback = action
            self._as.publish_feedback(self._feedback)
        else:
            rospy.loginfo('The input is not valid. Cancelling')
            # the following line, sets the client in preempted state (goal cancelled)
            self._as.set_preempted()
            success = False

        if self._as.is_preempt_requested():
            success = False
            rospy.loginfo('Action cancelled by the user')
            # the following line, sets the client in preempted state (goal cancelled)
            self._as.set_preempted()

        # at this point, either the goal has been achieved (success==true)
        # or the client preempted the goal (success==false)
        # If success, then we publish the final result
        # If not success, we do not publish anything in the result
        if success:
            rospy.loginfo('Succeeded in {}'.format(['taking off' if action == 'TAKEOFF' else 'landing']))
            self._as.set_succeeded()

if __name__ == '__main__':
    rospy.init_node('Takingoff_landing_server_node', anonymous=True)
    mySquareServer = ArdroneServerClass()
    mySquareServer.initialize_server()
    rospy.spin()