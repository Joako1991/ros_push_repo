#! /usr/bin/env python

import rospy
from std_msgs.msg import Int32 
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

class Controller(object):
    def __init__(self):
        self.right_range = 0
        self.central_range = 360
        self.left_range = 719
        # Distance measured in meters
        self.frontal_danger_distance = 1
        self.sides_danger_distance = 1
        # Speed in m/s
        self.max_speed = 1

    def run(self):
        # We create our cmd_vel publisher
        self.pub_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        self.sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, self.reading_callback)

    def determine_speeds(self, l_cmd, a_cmd):
        velocity_cmds = Twist()
        velocity_cmds.linear.x = l_cmd[0]
        velocity_cmds.linear.y = l_cmd[1]
        velocity_cmds.linear.z = l_cmd[2]
        velocity_cmds.angular.x = a_cmd[0]
        velocity_cmds.angular.y = a_cmd[1]
        velocity_cmds.angular.z = a_cmd[2]
        return velocity_cmds

    def reading_callback(self, msg):
        front_safe = msg.ranges[self.central_range] > self.frontal_danger_distance
        right_safe = msg.ranges[self.right_range] > self.sides_danger_distance
        left_safe  = msg.ranges[self.left_range] > self.sides_danger_distance
        linear_cmd = [0, 0, 0]
        angular_cmd = [0, 0, 0]

        coeff = 1
        if not front_safe:
            coeff = float(msg.ranges[self.central_range]) / self.frontal_danger_distance
            angular_cmd[2] = -1

        linear_cmd[0] = self.max_speed * (coeff ** 10)

        if not right_safe:
            angular_cmd[2] = -1

        if not left_safe:
            angular_cmd[2] = 1

        if not left_safe and not right_safe:
            if msg.ranges[self.right_range] > msg.ranges[self.left_range]:
                angular_cmd[2] = 1
            else:
                angular_cmd[2] = -1

        self.pub_vel.publish(self.determine_speeds(linear_cmd, angular_cmd))
        return

def main():
    # We create our main node
    rospy.init_node('topic_subscriber')
    my_robot_controller = Controller()
    print("Controller initialized")
    my_robot_controller.run()
    print("Blocking the node")
    rospy.spin()

if __name__ == '__main__':
    main()