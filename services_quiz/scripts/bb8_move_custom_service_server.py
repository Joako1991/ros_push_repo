#! /usr/bin/env python

import rospy
from services_quiz.srv import BB8CustomServiceMessage, BB8CustomServiceMessageResponse # you import the service message python classes generated from Empty.srv.
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from math import pi as M_PI
from tf.transformations import euler_from_quaternion


class BB8ServiceQuiz(object):
    def __init__(self):
        self.service_name = '/move_bb8_in_square_custom'
        # Speed in m/s
        self.forward_speed = 0.3
        # Angular speed in rad/s
        self.turning_speed = 0.1
        self.last_heading = 0.0
        self.kp = -0.5
        self.controller_rate = rospy.Rate(10.0)
        self.pi_4 = M_PI / 5.18
        self.controller_error = 0.01
        self.max_rot_speed = 0.2

    def initialize_service(self):
        # create the Service called my_service with the defined callback
        self.serv = rospy.Service(self.service_name, BB8CustomServiceMessage , self.bb_8_movement_callback)
        self.vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.subs = rospy.Subscriber ('/odom', Odometry, self.odom_callback)

    def odom_callback(self, state):
        self.last_heading = self.get_rotation(state)

    def fix_angle(self, angle):
        while angle < 0:
            angle += 2*M_PI
        while angle > 2*M_PI:
            angle -= 2*M_PI
        return angle

    def get_rotation(self, msg):
        global roll, pitch, yaw
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
        (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
        return self.fix_angle(yaw)

    def make_side(self, side_time, turning_time):
        speed_msg = Twist()
        speed_msg.linear.x = 0.0
        speed_msg.linear.y = 0.0
        speed_msg.linear.z = 0.0
        speed_msg.angular.x = 0.0
        speed_msg.angular.y = 0.0
        speed_msg.angular.z = 0.0

        print("Making the side")
        # We do one side
        speed_msg.linear.x = self.forward_speed
        speed_msg.angular.z = 0.0
        self.vel_pub.publish(speed_msg)
        rospy.sleep(side_time)

        print("Stopping the robot forward movement")
        # We stop the forward movement
        speed_msg.linear.x = 0.0
        speed_msg.angular.z = 0.0
        self.vel_pub.publish(speed_msg)
        rospy.sleep(4)

        print("Making the turn")
        target_heading = self.fix_angle(self.last_heading + self.pi_4)

        # We turn 90 degrees
        speed_msg.linear.x = 0.0

        while not rospy.is_shutdown() and abs(self.last_heading - target_heading) > self.controller_error:
            print("Target: {}. Actual: {}".format(target_heading, self.last_heading))
            angular = self.kp * (self.last_heading - target_heading)
            if angular > self.max_rot_speed:
                angular = self.max_rot_speed
            speed_msg.angular.z = angular
            self.vel_pub.publish(speed_msg)
            self.controller_rate.sleep()

        print("Stopping the robot turn")
        # We stop the turning movement
        speed_msg.linear.x = 0.0
        speed_msg.angular.z = 0.0
        self.vel_pub.publish(speed_msg)
        rospy.sleep(4)

    def make_square(self, side):
        side_time = side / self.forward_speed
        #  Pi / 4 is the turn we need to do at each corner
        turning_time = (M_PI / 4.5) / self.turning_speed
        print("Each side will take {} seconds and each turn will take {} sec".format(side_time, turning_time))

        counter = 0
        while counter < 4:
            self.make_side(side_time, turning_time)
            counter += 1

    def bb_8_movement_callback(self, request):
        while self.vel_pub.get_num_connections() == 0:
            rospy.sleep(1)

        side_val = request.side
        rep = request.repetitions
        print("Requested to make a square of {s} m of side, and {r} repetitions".format(s=side_val, r=rep))

        counter = 0
        while counter < rep:
            self.make_square(side_val)
            counter += 1

        response = BB8CustomServiceMessageResponse()
        response.success = True
        return response # the service Response class, in this case EmptyResponse

def main():
    my_obj = BB8ServiceQuiz()
    my_obj.initialize_service()
    rospy.spin() # maintain the service open.

if __name__ == '__main__':
    rospy.init_node('service_quiz_node_server') 
    main()