#! /usr/bin/env python

import rospkg
import rospy
from services_quiz.srv import BB8CustomServiceMessage, BB8CustomServiceMessageRequest # you import the service message python classes generated from Empty.srv.

import sys

# Initialise a ROS node with the name service_client
rospy.init_node('custom_service_client_node')

# Wait for the service client /move_bb8_in_circle to be running
rospy.wait_for_service('/move_bb8_in_square_custom')

# Create the connection to the service
bb_8_move_service = rospy.ServiceProxy('/move_bb8_in_square_custom', BB8CustomServiceMessage)

# Create an object of type TrajByNameRequest
bb_8_mov_obj = BB8CustomServiceMessageRequest()
bb_8_mov_obj.side = 1.5
bb_8_mov_obj.repetitions = 2

# Send through the connection the name of the trajectory to be executed by the robot
result = bb_8_move_service(bb_8_mov_obj)

# Print the result given by the service called
print("First round results: {}".format(result))

# Create an object of type TrajByNameRequest
bb_8_mov_obj = BB8CustomServiceMessageRequest()
bb_8_mov_obj.side = 3
bb_8_mov_obj.repetitions = 1

# Send through the connection the name of the trajectory to be executed by the robot
result = bb_8_move_service(bb_8_mov_obj)

# Print the result given by the service called
print("Second round results: {}".format(result))